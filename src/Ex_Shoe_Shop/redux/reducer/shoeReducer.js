import { dataShoe } from "../../dataShoe";
import { CHANGE_CART, CHANGE_DETAIL, CHANGE_NUM } from "../constant";

let initialState = {
  dataShoe: dataShoe,
  detail: dataShoe[0],
  cart: [],
};
export const shoeReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_DETAIL: {
      return { ...state, detail: action.item };
    }
    case CHANGE_CART: {
      let newcart = [...state.cart];
      let newItem = { ...action.item, number: 1 };
      let index = newcart.findIndex((item) => {
        return item.id == newItem.id;
      });
      if (index == -1) {
        newcart.push(newItem);
      } else {
        newcart[index].number++;
      }
      return { ...state, cart: newcart };
    }
    case CHANGE_NUM: {
      let newcart = [...state.cart];
      let index = newcart.findIndex((item) => {
        return item.id == action.item.id;
      });
      newcart[index].number += action.num;
      if (newcart[index].number == 0) {
        newcart.splice(index, 1);
      }
      return { ...state, cart: newcart };
    }
    default:
      return { ...state };
  }
};
