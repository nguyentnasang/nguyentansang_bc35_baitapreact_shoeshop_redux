import React, { Component } from "react";
import { connect } from "react-redux";
import { CHANGE_NUM } from "./redux/constant";

class Cart extends Component {
  rederTbody = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            <img width={35} src={item.image} alt="" />
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleChangeNum(item, +1);
              }}
              className="btn btn-info"
            >
              +
            </button>
            {item.number}
            <button
              onClick={() => {
                this.props.handleChangeNum(item, -1);
              }}
              className="btn btn-info"
            >
              -
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>NAME</th>
              <th>PRICE</th>
              <th>IMAGE</th>
              <th>QUANTITY</th>
            </tr>
          </thead>
          <tbody>{this.rederTbody()}</tbody>
        </table>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    cart: state.shoeReducer.cart,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleChangeNum: (item, num) => {
      dispatch({ type: CHANGE_NUM, item: item, num: num });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Cart);
