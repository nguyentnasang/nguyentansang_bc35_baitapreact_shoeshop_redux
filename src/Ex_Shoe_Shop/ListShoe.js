import React, { Component } from "react";
import { connect } from "react-redux";
import { CHANGE_CART, CHANGE_DETAIL } from "./redux/constant";

class ListShoe extends Component {
  renderListShoe = () => {
    return this.props.dataShoe.map((item) => {
      return (
        <div className="card col-4" style={{ width: "18rem" }}>
          <img className="card-img-top" src={item.image} alt="Card image cap" />
          <div className="card-body">
            <h5 className="card-title">{item.name}</h5>
            <p className="card-text">
              {item.description.length > 70
                ? item.description.substr(0, 70) + "..."
                : item.description}
            </p>
            <p>{item.price}$</p>
            <button
              onClick={() => {
                this.props.handleChangeDetail(item);
              }}
              className="btn btn-success mr-3"
            >
              xem chi tiet
            </button>
            <button
              onClick={() => {
                this.props.handleChangeCart(item);
              }}
              className="btn btn-success"
            >
              Thêm
            </button>
          </div>
        </div>
      );
    });
  };
  render() {
    return <div className="mt-5 row">{this.renderListShoe()}</div>;
  }
}
let mapStateToProps = (state) => {
  return {
    dataShoe: state.shoeReducer.dataShoe,
  };
};
let mapDisPatchToProps = (dispatch) => {
  return {
    handleChangeDetail: (item) => {
      dispatch({ type: CHANGE_DETAIL, item: item });
    },
    handleChangeCart: (item) => {
      dispatch({ type: CHANGE_CART, item: item });
    },
  };
};
export default connect(mapStateToProps, mapDisPatchToProps)(ListShoe);
