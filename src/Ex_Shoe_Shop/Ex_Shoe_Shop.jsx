import React, { Component } from "react";
import Cart from "./Cart";
import Detail from "./Detail";
import ListShoe from "./ListShoe";

export default class Ex_Shoe_Shop extends Component {
  render() {
    return (
      <div className="container">
        <Cart />
        <ListShoe />
        <Detail />
      </div>
    );
  }
}
