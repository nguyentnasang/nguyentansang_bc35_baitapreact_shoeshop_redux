import React, { Component } from "react";
import { connect } from "react-redux";

class Detail extends Component {
  render() {
    return (
      <div className="bg-secondary row p-4 mt-5">
        <img className="col-3" src={this.props.detail.image} alt="" />
        <div className="col-9 text-left text-white">
          <p>ID: {this.props.detail.id}</p>
          <p>NAME: {this.props.detail.name}</p>
          <p>ALIAS: {this.props.detail.alias}</p>
          <p>PRICE: {this.props.detail.price}$</p>
          <p>DESCRIPTION: {this.props.detail.description}</p>
        </div>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    detail: state.shoeReducer.detail,
  };
};
export default connect(mapStateToProps)(Detail);
